# potpourri - /pɒtˈpʊəri/

## Official Notebooks

### TensorFlow
  - [Lucid](https://github.com/tensorflow/lucid/tree/master/notebooks)
  - [Magenta](https://github.com/tensorflow/magenta-demos/tree/master/jupyter-notebooks)
  - [TF Hub](https://github.com/tensorflow/hub/tree/master/examples/colab)
  - [GCP + Keras + TPU](https://github.com/GoogleCloudPlatform/training-data-analyst/tree/master/courses/fast-and-lean-data-science)
    - [01_MNIST_TPU_Keras.ipynb](https://colab.research.google.com/github/GoogleCloudPlatform/training-data-analyst/blob/master/courses/fast-and-lean-data-science/01_MNIST_TPU_Keras.ipynb)
    - [06_MNIST_Estimator_to_TPUEstimator.ipynb](https://colab.research.google.com/github/GoogleCloudPlatform/training-data-analyst/blob/master/courses/fast-and-lean-data-science/06_MNIST_Estimator_to_TPUEstimator.ipynb)
    - [07_Keras_Flowers_TPU_squeezenet.ipynb](https://colab.research.google.com/github/GoogleCloudPlatform/training-data-analyst/blob/master/courses/fast-and-lean-data-science/07_Keras_Flowers_TPU_squeezenet.ipynb)
  - [TF 2.0 Updates](https://github.com/tensorflow/docs/tree/master/site/en/r2)
  - [MG - Course](https://github.com/GoogleCloudPlatform/training-data-analyst/tree/master/courses/fast-and-lean-data-science)

### JAX
  - [Intro](https://github.com/google/jax/tree/master/notebooks)
